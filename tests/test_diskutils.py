from diskutils import DiskUtils


class TestDiskUtils:

    def test_get_total_and_free_space_in_bytes_for_root_ok(self):
        """
        Test that everything goes well for "/" Mountpoint
        :return:
        """
        # We get ans instance of DiskUtils class
        o_diskutils = DiskUtils()

        b_success, i_total_size, i_free_size = o_diskutils.get_total_and_free_space_in_bytes("/")
        # Assert that type is Integer
        assert isinstance(b_success, bool) is True
        assert isinstance(i_total_size, int) is True
        assert isinstance(i_free_size, int) is True

        # Assert that values are consistent
        assert b_success is True
        assert i_total_size >= 0
        assert i_free_size >= 0
        assert i_free_size <= i_total_size

    def test_get_total_and_free_space_in_bytes_for_existing_ok(self):
        """
        Test that everything goes well for, another, existing, non root, like "/tmp" Mountpoint
        :return:
        """
        # We get ans instance of DiskUtils class
        o_diskutils = DiskUtils()

        b_success, i_total_size, i_free_size = o_diskutils.get_total_and_free_space_in_bytes("/tmp")
        # Assert that type is Integer
        assert isinstance(b_success, bool) is True
        assert isinstance(i_total_size, int) is True
        assert isinstance(i_free_size, int) is True

        # Assert that values are consistent
        assert b_success is True
        assert i_total_size >= 0
        assert i_free_size >= 0
        assert i_free_size <= i_total_size

    def test_get_total_and_free_space_in_bytes_for_non_existing_ko(self):
        """
        Test that errors are catch for a wrong, not existing, Mountpoint
        :return:
        """
        # We get ans instance of DiskUtils class
        o_diskutils = DiskUtils()

        b_success, i_total_size, i_free_size = o_diskutils.get_total_and_free_space_in_bytes("/does-not-exist")
        # Assert that type is Integer
        assert isinstance(b_success, bool) is True
        assert isinstance(i_total_size, int) is True
        assert isinstance(i_free_size, int) is True

        # Assert that values are consistent
        assert b_success is False
        assert i_total_size == 0
        assert i_free_size == 0

    def test_get_free_space_in_multiple_units(self):
        # We get ans instance of MyDisk class
        o_diskutils = DiskUtils()

        b_success, \
        i_free_size_in_bytes, \
        f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes = \
            o_diskutils.get_free_space_in_multiple_units("/")

        # Assert that types are right
        assert isinstance(b_success, bool) is True
        assert isinstance(i_free_size_in_bytes, int) is True
        assert isinstance(f_free_size_in_kbytes, (int, float)) is True
        assert isinstance(f_free_size_in_mbytes, (int, float)) is True
        assert isinstance(f_free_size_in_gbytes, (int, float)) is True
        assert isinstance(f_free_size_in_tbytes, (int, float)) is True

    def test_get_free_space_in_multiple_units_for_non_existing_ko(self):
        # We get ans instance of DiskUtils class
        o_diskutils = DiskUtils()

        b_success, \
        i_free_size_in_bytes, \
        f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes = \
            o_diskutils.get_free_space_in_multiple_units("/does-not-exist")

        # Assert that types are right
        assert isinstance(b_success, bool) is True
        assert isinstance(i_free_size_in_bytes, int) is True
        assert isinstance(f_free_size_in_kbytes, (int, float)) is True
        assert isinstance(f_free_size_in_mbytes, (int, float)) is True
        assert isinstance(f_free_size_in_gbytes, (int, float)) is True
        assert isinstance(f_free_size_in_tbytes, (int, float)) is True
