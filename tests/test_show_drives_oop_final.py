import pytest
from lib.pylibsubprocess import PyLibSubProcess
from lib.stringutils import StringUtils
from do.disk import Disk
from do.partition import Partition
from show_drives_oop_final import Drives


class TestDrives:

    # def test_def extract_disks(self, a_s_lines, h_s_i_fields):
    # def __init__(self, o_stringutils, o_pylibsubprocess):
    # def get_disks(self):
    # def print_disks(self, h_s_o_disks):
    # def main(self)

    a_output_1 = []
    a_output_1.append("NAME        HCTL       TYPE TRAN   VENDOR   MODEL                      REV FSSIZE FSTYPE     FSUSED FSUSE% MOUNTPOINT")
    a_output_1.append("loop0                  loop                                                135.8M squashfs   135.8M   100% /snap/chromium/1466")
    a_output_1.append("loop2                  loop                                                 97.9M squashfs    97.9M   100% /snap/core/10577")
    a_output_1.append("loop3                  loop                                                   98M squashfs      98M   100% /snap/core/10583")
    a_output_1.append("loop4                  loop                                                 55.4M squashfs    55.4M   100% /snap/core18/1932")
    a_output_1.append("loop5                  loop                                                 64.4M squashfs    64.4M   100% /snap/gtk-common-themes/1513")
    a_output_1.append("loop6                  loop                                                 64.9M squashfs    64.9M   100% /snap/gtk-common-themes/1514")
    a_output_1.append("loop7                  loop                                                  219M squashfs     219M   100% /snap/gnome-3-34-1804/66")
    a_output_1.append("loop8                  loop                                                141.1M squashfs   141.1M   100% /snap/vokoscreen-ng/94")
    a_output_1.append("loop9                  loop                                                 55.5M squashfs    55.5M   100% /snap/core18/1944")
    a_output_1.append("loop10                 loop                                                 51.1M squashfs    51.1M   100% /snap/snap-store/498")
    a_output_1.append("loop11                 loop                                                337.9M squashfs   337.9M   100% /snap/wine-platform-runtime/206")
    a_output_1.append("loop12                 loop                                                215.5M squashfs   215.5M   100% /snap/wine-platform-5-stable/13")
    a_output_1.append("loop13                 loop                                                141.1M squashfs   141.1M   100% /snap/vokoscreen-ng/86")
    a_output_1.append("loop14                 loop                                                 51.1M squashfs    51.1M   100% /snap/snap-store/518")
    a_output_1.append("loop15                 loop                                                  4.3M squashfs     4.3M   100% /snap/notepad-plus-plus/252")
    a_output_1.append("loop16                 loop                                                303.1M squashfs   303.1M   100% /snap/wine-platform-5-stable/16")
    a_output_1.append("loop17                 loop                                                  218M squashfs     218M   100% /snap/gnome-3-34-1804/60")
    a_output_1.append("loop18                 loop                                                  5.6M squashfs     5.6M   100% /snap/notepad-plus-plus/253")
    a_output_1.append("loop19                 loop                                                162.9M squashfs   162.9M   100% /snap/gnome-3-28-1804/145")
    a_output_1.append("loop20                 loop                                                337.6M squashfs   337.6M   100% /snap/wine-platform-runtime/200")
    a_output_1.append("loop21                 loop                                                135.8M squashfs   135.8M   100% /snap/chromium/1461")
    a_output_1.append("sda         2:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001")
    a_output_1.append("├─sda1                 part                                                       zfs_member")
    a_output_1.append("└─sda9                 part")
    a_output_1.append("sdb         3:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001")
    a_output_1.append("sdc         4:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001")
    a_output_1.append("├─sdc1                 part                                                       zfs_member")
    a_output_1.append("└─sdc9                 part")
    a_output_1.append("sdd         5:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001")
    a_output_1.append("├─sdd1                 part                                                       zfs_member")
    a_output_1.append("└─sdd9                 part")
    a_output_1.append("sde         6:0:0:0    disk sata   ATA      Samsung_SSD_860_EVO_1TB   4B6Q")
    a_output_1.append("└─sde1                 part                                                  468G ext4       375.2G    80% /home/carles/Desktop/Storage")
    a_output_1.append("nvme0n1                disk nvme            Samsung SSD 960 PRO 512GB")
    a_output_1.append("├─nvme0n1p1            part nvme                                             511M vfat         7.8M     2% /boot/efi")
    a_output_1.append("└─nvme0n1p2            part nvme                                             468G ext4       375.2G    80% /")

    h_s_i_index_titles = {'NAME': 0, 'HCTL': 12, 'TYPE': 23, 'TRAN': 28, 'VENDOR': 35, 'MODEL': 44, 'REV': 71, 'FSSIZE': 75, 'FSTYPE': 82, 'FSUSED': 93, 'FSUSE%': 100, 'MOUNTPOINT': 107}

    a_output_2 = []
    a_output_2.append("NAME        HCTL       TYPE TRAN   VENDOR   MODEL                      REV FSSIZE FSTYPE     FSUSED FSUSE% MOUNTPOINT")

    h_s_i_index_titles2 = {'NAME': 0, 'HCTL': 12, 'TYPE': 23, 'TRAN': 28, 'VENDOR': 35, 'MODEL': 44, 'REV': 71, 'FSSIZE': 75, 'FSTYPE': 82,
                           'FSUSED': 93, 'FSUSE%': 100, 'MOUNTPOINT': 107}


    a_output_3 = []
    a_output_3.append("NAME        HCTL       TYPE TRAN   VENDOR   MODEL                      REV FSSIZE FSTYPE     FSUSED FSUSE% MOUNTPOINT")
    a_output_3.append("sda         2:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001")
    a_output_3.append("├─sda1                 part                                                       zfs_member")

    h_s_i_index_titles3 = {'NAME': 0, 'HCTL': 12, 'TYPE': 23, 'TRAN': 28, 'VENDOR': 35, 'MODEL': 44, 'REV': 71, 'FSSIZE': 75, 'FSTYPE': 82,
                           'FSUSED': 93, 'FSUSE%': 100, 'MOUNTPOINT': 107}


    s_output_run = """NAME        HCTL       TYPE TRAN   VENDOR   MODEL                      REV FSSIZE FSTYPE     FSUSED FSUSE% MOUNTPOINT
loop0                  loop                                                135.8M squashfs   135.8M   100% /snap/chromium/1466
loop2                  loop                                                 97.9M squashfs    97.9M   100% /snap/core/10577
loop3                  loop                                                   98M squashfs      98M   100% /snap/core/10583
loop4                  loop                                                 55.4M squashfs    55.4M   100% /snap/core18/1932
loop5                  loop                                                 64.4M squashfs    64.4M   100% /snap/gtk-common-themes/1513
loop6                  loop                                                 64.9M squashfs    64.9M   100% /snap/gtk-common-themes/1514
loop7                  loop                                                  219M squashfs     219M   100% /snap/gnome-3-34-1804/66
loop8                  loop                                                141.1M squashfs   141.1M   100% /snap/vokoscreen-ng/94
loop9                  loop                                                 55.5M squashfs    55.5M   100% /snap/core18/1944
loop10                 loop                                                 51.1M squashfs    51.1M   100% /snap/snap-store/498
loop11                 loop                                                337.9M squashfs   337.9M   100% /snap/wine-platform-runtime/206
loop12                 loop                                                215.5M squashfs   215.5M   100% /snap/wine-platform-5-stable/13
loop13                 loop                                                141.1M squashfs   141.1M   100% /snap/vokoscreen-ng/86
loop14                 loop                                                 51.1M squashfs    51.1M   100% /snap/snap-store/518
loop15                 loop                                                  4.3M squashfs     4.3M   100% /snap/notepad-plus-plus/252
loop16                 loop                                                303.1M squashfs   303.1M   100% /snap/wine-platform-5-stable/16
loop17                 loop                                                  218M squashfs     218M   100% /snap/gnome-3-34-1804/60
loop18                 loop                                                  5.6M squashfs     5.6M   100% /snap/notepad-plus-plus/253
loop19                 loop                                                162.9M squashfs   162.9M   100% /snap/gnome-3-28-1804/145
loop20                 loop                                                337.6M squashfs   337.6M   100% /snap/wine-platform-runtime/200
loop21                 loop                                                135.8M squashfs   135.8M   100% /snap/chromium/1461
sda         2:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001                                 
├─sda1                 part                                                       zfs_member               
└─sda9                 part                                                                                
sdb         3:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001                                 
sdc         4:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001                                 
├─sdc1                 part                                                       zfs_member               
└─sdc9                 part                                                                                
sdd         5:0:0:0    disk sata   ATA      ST2000LM015-2E8174        0001                                 
├─sdd1                 part                                                       zfs_member               
└─sdd9                 part                                                                                
sde         6:0:0:0    disk sata   ATA      Samsung_SSD_860_EVO_1TB   4B6Q                                 
└─sde1                 part                                                  468G ext4       375.2G    80% /home/carles/Desktop/Storage
nvme0n1                disk nvme            Samsung SSD 960 PRO 512GB                                      
├─nvme0n1p1            part nvme                                             511M vfat         7.8M     2% /boot/efi
└─nvme0n1p2            part nvme                                             468G ext4       375.2G    80% /
"""

    def test_constructor(self):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        assert isinstance(o_stringutils, StringUtils) is True
        assert isinstance(o_pylibsubprocess, PyLibSubProcess) is True
        assert isinstance(o_drives, Drives) is True
        assert isinstance(o_drives.o_stringutils, StringUtils) is True
        assert isinstance(o_drives.o_pylibsubprocess, PyLibSubProcess) is True

    def test_extract_disks(self):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        h_s_o_disks = o_drives.extract_disks(self.a_output_1, self.h_s_i_index_titles)

        assert len(h_s_o_disks) == 6
        for s_diskname in h_s_o_disks:
            assert isinstance(h_s_o_disks[s_diskname], Disk)

        assert len(h_s_o_disks['sda'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sdb'].h_s_o_partitions) == 0
        assert len(h_s_o_disks['sdc'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sdd'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sde'].h_s_o_partitions) == 1
        assert len(h_s_o_disks['nvme0n1'].h_s_o_partitions) == 2

    def test_extract_disks_no_disks(self):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        h_s_o_disks = o_drives.extract_disks(self.a_output_2, self.h_s_i_index_titles2)

        assert len(h_s_o_disks) == 0

    def test_extract_disks_one_disk_one_partition(self):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        h_s_o_disks = o_drives.extract_disks(self.a_output_3, self.h_s_i_index_titles3)

        assert len(h_s_o_disks) == 1
        assert len(h_s_o_disks['sda'].h_s_o_partitions) == 1

    def test_get_disks(self, monkeypatch):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            b_success = True
            b_timeout = False
            s_stdout = self.s_output_run
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        b_error, h_s_o_disks = o_drives.get_disks()
        assert b_error is False
        assert len(h_s_o_disks) == 6
        for s_diskname in h_s_o_disks:
            assert isinstance(h_s_o_disks[s_diskname], Disk)

        assert len(h_s_o_disks['sda'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sdb'].h_s_o_partitions) == 0
        assert len(h_s_o_disks['sdc'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sdd'].h_s_o_partitions) == 2
        assert len(h_s_o_disks['sde'].h_s_o_partitions) == 1
        assert len(h_s_o_disks['nvme0n1'].h_s_o_partitions) == 2

        assert h_s_o_disks['nvme0n1'].h_s_o_partitions['nvme0n1p1'].s_fstype == "vfat"
        assert h_s_o_disks['nvme0n1'].h_s_o_partitions['nvme0n1p2'].s_fstype == "ext4"
        assert h_s_o_disks['nvme0n1'].h_s_o_partitions['nvme0n1p2'].s_mountpoint == "/"

    def test_get_disks_error(self, monkeypatch):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            # We return error
            b_success = False
            b_timeout = False
            s_stdout = ""
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        b_error, h_s_o_disks = o_drives.get_disks()
        assert b_error is True
        assert len(h_s_o_disks) == 0

    def test_get_disks_ok_error_titles_empty(self, monkeypatch):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            # We return error
            b_success = True
            b_timeout = False
            s_stdout = ""
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        b_error, h_s_o_disks = o_drives.get_disks()
        assert b_error is True
        assert len(h_s_o_disks) == 0

    def test_get_disks_ok_error_titles_not_empty(self, monkeypatch):
        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            # We return error
            b_success = True
            b_timeout = False
            s_stdout = "This version is out to date. Please update it"
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        b_error, h_s_o_disks = o_drives.get_disks()
        assert b_error is True
        assert len(h_s_o_disks) == 0

    def test_main(self, monkeypatch, capsys):

        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            b_success = True
            b_timeout = False
            s_stdout = self.s_output_run
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        o_drives.main()

        o_captured = capsys.readouterr()

        assert o_captured.out == """6 disks founds
sda sata ST2000LM015-2E8174 Partitions: 2
sdb sata ST2000LM015-2E8174 Partitions: 0
sdc sata ST2000LM015-2E8174 Partitions: 2
sdd sata ST2000LM015-2E8174 Partitions: 2
sde sata Samsung_SSD_860_EVO_1TB Partitions: 1
nvme0n1 nvme Samsung SSD 960 PRO 512GB Partitions: 2
"""
        assert o_captured.err == ""


    def test_main_error(self, monkeypatch, capsys):

        o_stringutils = StringUtils()
        o_pylibsubprocess = PyLibSubProcess()
        o_drives = Drives(o_stringutils=o_stringutils, o_pylibsubprocess=o_pylibsubprocess)

        def mock_subprocess_return(o_pylibsubprocess, a_parameters, i_timeout):
            b_success = False
            b_timeout = False
            s_stdout = self.s_output_run
            s_stderr = ""
            return b_success, b_timeout, s_stdout, s_stderr

        monkeypatch.setattr(PyLibSubProcess, 'run', mock_subprocess_return)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_drives.main()

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

        o_captured = capsys.readouterr()

        assert o_captured.out == "There has been an error.\n"
        assert o_captured.err == ""
