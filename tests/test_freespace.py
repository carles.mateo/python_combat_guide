from freespace import FreeSpace
from lib.diskutils import DiskUtils
from lib.stringutils import StringUtils


class TestFreeSpace:

    def test_get_free_space_formatted_on_root(self):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space != ""

    def test_get_free_space_formatted_mock_5tb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            i_free_size_in_bytes = 5 * 1024 * 1024 * 1024 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "5.00 TB"
        assert s_warnings == ""

    def test_get_free_space_formatted_mock_5_10tb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            i_free_size_in_bytes = 5 * 1024 * 1024 * 1024 * 1024
            # We add 100 GB so we have 5.10 TB
            i_free_size_in_bytes = i_free_size_in_bytes + 100 * 1024 * 1024 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "5.10 TB"
        assert s_warnings == ""

    def test_get_free_space_formatted_mock_5_tb_and_1_byte(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            i_free_size_in_bytes = (5 * 1024 * 1024 * 1024 * 1024) + 1
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "5.00 TB"
        assert s_warnings == ""

    def test_get_free_space_formatted_mock_9_87gb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 9.87 GB
            i_free_size_in_bytes = 9.87 * 1024 * 1024 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "9.87 GB"
        assert s_warnings == ""

    def test_get_free_space_formatted_mock_999gb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 999 GB
            i_free_size_in_bytes = 999 * 1024 * 1024 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "999.00 GB"
        assert s_warnings == ""

    def test_get_free_space_formatted_mock_9mb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 9 MB
            i_free_size_in_bytes = 9 * 1024 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "9.00 MB"
        assert s_warnings == "You are really running out of space...\n"

    def test_get_free_space_formatted_mock_1kb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 1 KB
            i_free_size_in_bytes = 1 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "1.00 KB"
        assert s_warnings == "You have only few Kbytes left!\n"

    def test_get_free_space_formatted_mock_1_5kb(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 1 KB
            i_free_size_in_bytes = 1.5 * 1024
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "1.50 KB"
        assert s_warnings == "You have only few Kbytes left!\n"

    def test_get_free_space_formatted_mock_7b(self, monkeypatch):
        # Prepare the dependencies
        o_diskutils = DiskUtils()
        o_stringutils = StringUtils()

        def mock_get_free_space_in_multiple_units(DiskUtils, s_path):
            b_success = True
            # 1 KB
            i_free_size_in_bytes = 7
            f_free_size_in_kbytes = i_free_size_in_bytes / 1024
            f_free_size_in_mbytes = f_free_size_in_kbytes / 1024
            f_free_size_in_gbytes = f_free_size_in_mbytes / 1024
            f_free_size_in_tbytes = f_free_size_in_gbytes / 1024
            return b_success, \
                   i_free_size_in_bytes, \
                   f_free_size_in_kbytes, f_free_size_in_mbytes, f_free_size_in_gbytes, f_free_size_in_tbytes

        monkeypatch.setattr(DiskUtils, 'get_free_space_in_multiple_units', mock_get_free_space_in_multiple_units)

        # Inject the dependencies
        o_freespace = FreeSpace(o_diskutils=o_diskutils, o_stringutils=o_stringutils)

        b_success, s_free_space, s_warnings = o_freespace.get_free_space_formatted("/")
        assert b_success is True
        assert isinstance(s_free_space, str)
        assert s_free_space == "7 Bytes"
        assert s_warnings == "You are actually really running out of any space\n"
