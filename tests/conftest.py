# conftest.py sample file
# Please note: You cannot rename parameter item

import pytest
import time


@pytest.fixture
def nosleep(monkeypatch):
    def sleep(i_seconds):
        pass

    monkeypatch.setattr(time, 'sleep', sleep)


def pytest_runtest_setup(item):
    """
    This Hook will be called before ``pytest_runtest_call(item).
    We could setup test files, etc...
    """
    print("Running Test: " + str(item))
