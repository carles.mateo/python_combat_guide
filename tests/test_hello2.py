class Hello:

    def hello(self, s_name=""):
        """Returns Hello yourname or Hello World!
        :return String
        """
        if s_name == "":
            return "Hello World!"

        return "Hello " + s_name


class TestHello:

    def test_hello(self):
        o_hello = Hello()

        s_result = o_hello.hello("Michela")
        assert s_result == "Hello Michela"

        s_result = o_hello.hello()
        assert s_result == "Hello World!"
