#
# Tests for File class
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
#

import pytest
import os
from fileutils import FileUtils


@pytest.fixture
def open_raises_ioerror(monkeypatch):
    def mock_test_helper(o_self):
        raise IOError

    monkeypatch.setattr(FileUtils, 'test_helper', mock_test_helper)


class TestFile(object):

    s_path_test_file = "/tmp/test_file.txt"
    s_sample_file_text = "For testing purposes\nA second line"

    def create_sample_file(self):
        """
        It creates a sample file. It is used for certain tests.
        :return:
        """
        o_file = FileUtils()

        b_success = o_file.write(self.s_path_test_file, self.s_sample_file_text)

    # Start Tests
    def test_append_ok(self):
        o_file = FileUtils()
        b_result = o_file.append("/tmp/test_file.txt", "Test from test_file.txt")

        assert b_result is True

    def test_append_ko(self):
        o_file = FileUtils()
        b_result = o_file.append("/not_existing/test_file.txt", "Test from test_file.txt")

        assert b_result is False

    def test_append_ioerror_exception(self, open_raises_ioerror):
        o_file = FileUtils()
        b_success = o_file.append("/tmp/test_file.txt", "Test from test_file.txt")

        assert b_success is False

    def test_write_ok(self):
        o_file = FileUtils()
        b_result = o_file.write("/tmp/test_file.txt", "Test from test_file.txt")

        assert b_result is True

    def test_write_ko(self):
        o_file = FileUtils()
        b_result = o_file.write("/not_existing/test_file.txt", "Test from test_file.txt")

        assert b_result is False

    def test_read_ok(self):
        o_file = FileUtils()
        b_result, s_text = o_file.read_file("/tmp/test_file.txt")

        assert b_result is True

    def test_read_ko(self):
        o_file = FileUtils()
        b_result, s_text = o_file.read_file("/not_existing/test_file.txt")

        assert b_result is False

    def test_get_read_binary(self):
        o_file = FileUtils()

        s_file = "/tmp/binary.test"

        b_result = o_file.write(s_file, "ABC")
        assert b_result is True

        b_result, a_binary_content = o_file.read_binary(s_file)
        assert b_result is True

        s_text_content = ""
        for i_char in a_binary_content:
            if i_char > 40 and i_char < 123:
                s_text_content += chr(i_char)

        assert s_text_content == "ABC"

    def test_delete_ko(self):
        o_file = FileUtils()
        b_result = o_file.delete("/not_existing/test_file.txt")

        assert b_result is False

    def test_delete_ok(self):
        self.create_sample_file()

        o_file = FileUtils()
        b_result = o_file.delete(self.s_path_test_file)

        assert b_result is True

    def test_delete_ioerror_exception(self, open_raises_ioerror):
        self.create_sample_file()

        o_file = FileUtils()
        b_result = o_file.delete(self.s_path_test_file)

        assert b_result is False


    def test_delete_with_mask(self):
        self.create_sample_file()

        o_file = FileUtils()
        b_success, i_files_deleted = o_file.delete_all_with_mask(self.s_path_test_file)

        assert b_success is True
        assert i_files_deleted == 1

    def test_folder_exists(self):
        s_path_folder = "/tmp/"

        o_file = FileUtils()

        b_result = o_file.folder_exists(s_path_folder)
        assert b_result is True

        s_path_folder = "/tmp"
        b_result = o_file.folder_exists(s_path_folder)
        assert b_result is True

        s_path_folder = "/this-has-to-fail"
        b_result = o_file.folder_exists(s_path_folder)
        assert b_result is False

    def test_file_exists(self):
        s_path_file = "/tmp/not_real_file.txt"

        o_file = FileUtils()

        b_result = o_file.file_exists(s_path_file)
        assert b_result is False

        # create file to test for existing file
        o_file.write("/tmp/test_file.txt", "File for testing")

        s_path_file = "/tmp/test_file.txt"
        b_result = o_file.file_exists(s_path_file)
        assert b_result is True

        # Clean up the file
        o_file.delete("/tmp/test_file.txt")

    def test_create_folder_ok(self):
        s_path_folder = "/tmp/test"

        o_file = FileUtils()

        b_result = o_file.create_folder(s_path_folder)
        assert b_result is True

        b_result = o_file.remove_folder(s_path_folder)
        assert b_result is True

    def test_create_folder_ko(self):
        s_path_folder = "/tmp/test"

        o_file = FileUtils()

        s_path_folder = "Ç://///this-should-give-exception-catched"
        b_result = o_file.create_folder(s_path_folder)
        assert b_result is False

    def test_path_exists(self):
        s_path = "/tmp"

        o_file = FileUtils()

        b_result = o_file.path_exists(s_path)
        assert b_result is True

        # test with file
        s_path = s_path + "/file.txt"
        o_file.write(s_path, "File for testing")

        b_result = o_file.path_exists(s_path)
        assert b_result is True

        # test with bad path
        s_path = "/idontexist"
        b_result = o_file.path_exists(s_path)
        assert b_result is False

    def test_list_dir(self):
        s_path = "/tmp"

        o_file = FileUtils()

        # create a file for the test
        filename = "testfile"
        o_file.write(s_path + "/" + filename, "File for testing")

        b_success, a_files = o_file.list_dir(s_path)

        assert b_success is True
        assert filename in a_files

        # test with bad path
        s_path = "/idontexist"

        b_success, a_files = o_file.list_dir(s_path)
        assert b_success is False
        assert len(a_files) == 0

    def test_get_real_path(self):
        s_path = "/tmp"

        o_file = FileUtils()

        # create a file for the test
        filename = s_path + "/testfile"
        o_file.write(s_path + "/" + filename, "File for testing")

        # create a symbolic link
        s_sym_link = s_path + "/testsymlink"
        if not o_file.path_exists(s_sym_link):
            os.symlink(filename, s_sym_link)

        b_success, s_real_path = o_file.get_real_path(s_sym_link)
        assert b_success is True
        assert s_real_path == filename

        # try with non symlink path
        b_success, s_real_path = o_file.get_real_path(filename)
        assert b_success is True
        assert s_real_path == filename

        # try with bad path
        b_success, s_real_path = o_file.get_real_path("/tmp/idontexist")
        assert b_success is False

    def test_get_all_with_mask(self):
        o_file = FileUtils()

        s_mask = "/etc/*nam*"

        b_success, l_files = o_file.get_all_files_or_directories_with_mask(s_mask)

        assert b_success is True
        assert len(l_files) > 0

    def test_write_if_changed(self, monkeypatch):
        o_file = FileUtils()

        # test with file that does not exist, the file should be created
        b_success, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_success is True
        assert s_err == ""

        # test with same contents
        b_success, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_success is False
        assert s_err == "The contents are the same."

        # test with updated contents
        b_success, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text + "extra content")
        assert b_success is True
        assert s_err == ""

        # test with read failure
        def mock_read(s_file_name):
            return False, ""
        monkeypatch.setattr(o_file, 'read_file', mock_read)

        b_success, s_err = o_file.write_if_changed(self.s_path_test_file, self.s_sample_file_text)
        assert b_success is False
        assert s_err == "Unable to read File."

        # clean up
        o_file.delete(self.s_path_test_file)
