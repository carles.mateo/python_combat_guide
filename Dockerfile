FROM ubuntu:20.04

MAINTAINER Carles Mateo

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y vim python3-pip &&  pip3 install pytest && apt-get clean

ENV PYTHON_COMBAT_GUIDE /var/python_combat_guide

RUN mkdir -p $PYTHON_COMBAT_GUIDE

COPY ./ $PYTHON_COMBAT_GUIDE

ENV PYTHONPATH "${PYTHONPATH}:$PYTHON_COMBAT_GUIDE/src/:$PYTHON_COMBAT_GUIDE/src/lib"

# This is important so when executing python3 -m current directory will be added to Syspath
# Is not necessary, as we added to PYTHONPATH
#WORKDIR $PYTHON_COMBAT_GUIDE/src/lib

# To just execute this test, uncomment it, and comment the global one, in the bottom
#CMD ["/usr/bin/python3", "-m", "pytest", "/var/python_combat_guide/tests/test_mydisk.py"]

# Uncomment this line for testing all the tests
CMD ["/usr/bin/python3", "-m", "pytest", "/var/python_combat_guide/tests/"]