
def number_translate(i_index):
    """
    Translates a number from 0 to 9 to an English word
    :param i_index: The number to print, which will also be the index in the Array
    :return: String: Number translated to a word
    """
    if i_index < 0 or i_index > 9:
        return "Index is out of range"

    a_numbers =[]
    a_numbers.append("zero")
    a_numbers.append("one")
    a_numbers.append("two")
    a_numbers.append("three")
    a_numbers.append("four")
    a_numbers.append("five")
    a_numbers.append("six")
    a_numbers.append("seven")
    a_numbers.append("eight")
    a_numbers.append("nine")

    return a_numbers[i_index]


if __name__ == "__main__":
    print(number_translate(9))
    assert number_translate(9) == "nine"
