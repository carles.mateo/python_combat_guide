
def first_char(s_text, s_char):
    i_position = 0

    for s_text_char in s_text:
        if s_text_char == s_char:
            return i_position

        i_position = i_position + 1

    return -1


if __name__ == "__main__":
    s_text_to_search = "Jackie is a name that can refer to men or women, I’m not talking about your friend Jackie, but to Jackie Chan"
    s_char_to_search = "I"

    i_first_index = first_char(s_text_to_search, s_char_to_search)
    if i_first_index < 0:
        print("The char", s_char_to_search, "was not found in:", s_text_to_search)
    else:
        print("The first occurrence of", s_char_to_search, "in:", s_text_to_search, "was position:", i_first_index)
