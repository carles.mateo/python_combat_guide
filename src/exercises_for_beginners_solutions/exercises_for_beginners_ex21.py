
def convert_bytes_to_kbytes(i_bytes):
    """
    Convert integer number of bytes to kbytes rounded with two decimals
    :param i_bytes: integer, amount bytes to convert to kbytes
    :return: float
    """
    f_kbytes = i_bytes/1024
    f_kbytes = round(f_kbytes, 2)

    return f_kbytes


if __name__ == "__main__":
    print(convert_bytes_to_kbytes(1138))
