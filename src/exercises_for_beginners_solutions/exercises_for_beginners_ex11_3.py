
if __name__ == "__main__":
    i_index = 0
    i_times_found = 0

    s_text = "Jackie is a name that can refer to men or women, I’m not talking about your friend Jackie, but to Jackie Chan"
    s_char_searched = "I"

    for s_text_char in s_text:
        if s_text_char == s_char_searched:
            print("Found", s_char_searched, "in position", i_index)
            i_times_found = i_times_found + 1

        i_index = i_index + 1

    print("The character", s_char_searched, "was found", i_times_found, "time/s.")
