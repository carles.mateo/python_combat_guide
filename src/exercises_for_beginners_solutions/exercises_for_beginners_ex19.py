def translate_numbers(i_number):
    """"
    Will convert a number Integer to a String with the name.
    :param i_number: The Integer number
    :return: s_number
    """

    a_s_names = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

    if i_number < -9 or i_number > 9:
        s_number = "unknown"
    elif i_number < 0 and i_number > -10:
        i_number_positive = abs(i_number)
        s_number = "minus " + a_s_names[i_number_positive]
    else:
        s_number = a_s_names[i_number]

    return s_number


if __name__ == "__main__":
    # Test -1
    print(translate_numbers(-1))
    # Test -9
    print(translate_numbers(-9))
    # Test 0
    print(translate_numbers(0))
    # Test 7
    print(translate_numbers(7))
    # Test 11 (invalid)
    print(translate_numbers(11))
