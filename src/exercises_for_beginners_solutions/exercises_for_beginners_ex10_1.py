from exercises_for_beginners_ex10 import number_translate

if __name__ == "__main__":
    s_number_to_read = "911"

    for s_char in s_number_to_read:
        i_num = int(s_char)

        s_number = number_translate(i_num)
        print(s_number, end=" ")
