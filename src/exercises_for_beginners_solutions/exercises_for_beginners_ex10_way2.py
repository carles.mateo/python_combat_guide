
def get_numbers_in_language(s_lang="en"):
    """
    Returns an array with the numbers in the Language
    :param s_lang:
    :return: Array: a_numbers
    """

    a_numbers = []

    if s_lang == "en":
        a_numbers.append("zero")
        a_numbers.append("one")
        a_numbers.append("two")
        a_numbers.append("three")
        a_numbers.append("four")
        a_numbers.append("five")
        a_numbers.append("six")
        a_numbers.append("seven")
        a_numbers.append("eight")
        a_numbers.append("nine")

    if s_lang == "ca":
        a_numbers.append("zero")
        a_numbers.append("un")
        a_numbers.append("dos")
        a_numbers.append("tres")
        a_numbers.append("quatre")
        a_numbers.append("cinc")
        a_numbers.append("sis")
        a_numbers.append("set")
        a_numbers.append("vuit")
        a_numbers.append("nou")

    if s_lang == "es":
        a_numbers.append("cero")
        a_numbers.append("uno")
        a_numbers.append("dos")
        a_numbers.append("tres")
        a_numbers.append("cuatro")
        a_numbers.append("cinco")
        a_numbers.append("seis")
        a_numbers.append("siete")
        a_numbers.append("ocho")
        a_numbers.append("nueve")

    return a_numbers


def number_translate(i_index, s_lang="en"):
    """
    Translates a number from 0 to 9 to an English/Catalan/Spanish word
    :param i_index: The number to print, which will also be the index in the Array
    :return: String: Number translated to a word
    """
    if i_index < 0 or i_index > 9:
        return "Index is out of range"

    if s_lang != "en" and s_lang != "ca" and s_lang != "es":
        return "Language unknown"

    a_numbers = get_numbers_in_language(s_lang)

    return a_numbers[i_index]


if __name__ == "__main__":
    print(number_translate(9, s_lang="en"))
    assert number_translate(1, s_lang="en") == "one"
    assert number_translate(9, s_lang="en") == "nine"

    print(number_translate(9, s_lang="ca"))
    assert number_translate(1, s_lang="ca") == "un"
    assert number_translate(9, s_lang="ca") == "nou"

    print(number_translate(9, s_lang="es"))
    assert number_translate(1, s_lang="es") == "uno"
    assert number_translate(9, s_lang="es") == "nueve"

    assert number_translate(1, s_lang="zz") == "Language unknown"
    assert number_translate(10, s_lang="zz") == "Index is out of range"
