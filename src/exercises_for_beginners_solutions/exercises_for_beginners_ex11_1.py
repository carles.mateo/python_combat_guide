def first_index(s_text, s_search):
    """
    Will return the first position in the String s_text, where the String s_search begins, or -1 if it is not found
    :param s_text:
    :param s_search:
    :return: integer
    """

    i_position = s_text.find(s_search)

    return i_position


if __name__ == "__main__":
    print(first_index("Jackie is a name that can refer to men or women, I'm not talking about your friend Jackie, but to Jackie Chan", "Jackie"))
