def last_index(s_full_text, s_substring):
    """
    Return the position of the last occurrence of the String s_subtring in the String s_full_text
    :param s_full_text:  The full text containing none, one or more times the substring
    :param s_substring:  The substring that we want to search for in s_full_text
    :return: int: The position or -1 if it was not found
    """
    return s_full_text.rfind(s_substring)

print(last_index("Jackie is a name that can refer to men or women, I’m not talking about your friend Jackie, but to Jackie name in general", "Jackie"))

print(last_index("Jackie és un nom que tant pot referir a homes com a dones, no parlo de la teva amiga Jackie, si no del nom Jackie en general", "Jackie"))

print(last_index("Jackie es un nombre que tanto puede referirse a hombres como a mujeres, no hablo de tu amiga Jackie, si no del nombre Jackie en general", "Jackie"))

assert last_index("Jackie is a name that can refer to men or women, I’m not talking about your friend Jackie, but to Jackie name in general", "Jackie") == 98, "The function has an error!"
assert last_index("Jackie és un nom que tant pot referir a homes com a dones, no parlo de la teva amiga Jackie, si no del nom Jackie en general", "Jackie") == 107, "La funció te un error!"
assert last_index("Jackie es un nombre que tanto puede referirse a hombres como a mujeres, no hablo de tu amiga Jackie, si no del nombre Jackie en general", "Jackie") == 118, "La función tiene un error!"
assert last_index("Jackie is a name that can refer to men or women, I’m not talking about your friend Jackie, but to Jackie name in general", "Maria") == -1, "The function has an error!"
