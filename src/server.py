#
# server.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Sample class to manage Servers
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import os
import subprocess
import time


class Server:
    s_hostname = ""
    s_user = ""
    s_kernel_version = ""
    s_administration_ip = ""
    s_status = ""

    POWERED_ON = "Powered ON"
    POWERED_OFF = "Powered OFF"
    POWERING_OFF = "Powering OFF"
    REBOOTING = "Rebooting"
    GETTING_SYS_INFO = "Getting System Info"
    UPGRADING = "Upgrading"

    def __init__(self, s_hostname, s_administration_ip, s_user):
        self.s_hostname = s_hostname
        self.s_user = s_user
        self.s_administration_ip = s_administration_ip
        self.s_kernel_version = self.update_kernel_info()

    def send_poweroff(self):
        self.s_status = self.POWERING_OFF
        s_command = "ssh " + self.s_user + "@" + self.s_administration_ip + " 'poweroff'"
        os.system(s_command)
        time.sleep(10)
        self.s_status = self.POWERED_OFF

    def send_reboot(self):
        self.s_status = self.REBOOTING
        s_command = "ssh " + self.s_user + "@" + self.s_administration_ip + " 'reboot'"
        os.system(s_command)
        time.sleep(10)
        self.s_status = self.POWERED_ON

    def update_kernel_info(self):
        self.s_status = self.GETTING_SYS_INFO
        # Enforcing the use of a specific Key for Server
        s_cert_path = "/var/certs/id_rsa-" + self.s_administration_ip
        o_ssh = subprocess.Popen(["ssh", "-i " + s_cert_path, self.s_user + "@" + self.s_administration_ip],
                                 stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 universal_newlines=True,
                                 bufsize=0)

        # Send ssh commands to stdin
        o_ssh.stdin.write("uname --kernel-release\n")
        o_ssh.stdin.close()

        # Fetch output
        s_kernel_info = ""
        for s_line in o_ssh.stdout:
            s_kernel_info = s_kernel_info + s_line.strip()
            # Add a jup of line at the end
            s_kernel_info = s_kernel_info + "\n"

        self.s_status = self.POWERED_ON

        return s_kernel_info

    def send_upgrade_kernel(self, s_kernel="linux-modules-extra-4.15.0-91-generic"):
        self.s_status = self.UPGRADING
        s_command = "ssh " + self.s_user + "@" + self.s_administration_ip + " " + \
                    "'apt update && apt install -y " + s_kernel + "'"
        os.system(s_command)
        self.s_status = self.POWERED_ON

    def get_hostname(self):
        return self.s_hostname

    def get_kernel_version(self):
        return self.s_kernel_version

    def get_status(self):
        return self.s_status
