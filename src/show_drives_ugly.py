#!/usr/bin/env python3

import subprocess

# Please note, it is safer to do:
# out = subprocess.run(["lsblk", "--scsi"], shell=False)
# Note also: we should be catching the Exception
# Finally: Timed out parameter can be interesting to be used as long as we know that killing the process for timed out
#          will not cause any damage.
out = subprocess.run('lsblk -o NAME,HCTL,TYPE,TRAN,VENDOR,MODEL,REV,FSSIZE,FSTYPE,FSUSED,FSUSE%,MOUNTPOINT | grep -v "loop"', shell=True, capture_output=True)

print("{}".format(out.stdout.decode()))
