if __name__ == '__main__':
    i_n = int(input())
    i_initial_value = 1
    i_total = 0
    # Precalculate addition_power (to store enough space for the number to be printed like if it was a string)
    i_additional_power = 0
    for i_number in range(i_initial_value, i_n + 1):
        i_number_divided = i_number
        while i_number_divided > 9:
            i_additional_power = i_additional_power + 1
            i_number_divided = i_number_divided // 10

    for i_number in range(i_initial_value, i_n + 1):
        i_multiplier = i_number * (10 ** ((i_n - i_number) + i_additional_power))
        i_number_divided = i_number
        i_number_of_zeros = 0
        while i_number_divided > 9:
            i_number_of_zeros = i_number_of_zeros + 1
            i_multiplier = int(i_multiplier // (10 ** (i_number + 1 - (10 ** i_number_of_zeros))))
            i_number_divided = i_number_divided // 10

        i_total = i_total + i_multiplier
    print(i_total)