#
# server.py
#
# Author: Carles Mateo
# Creation Date: 2014-01-01
# Description: Sample example about what you should not do. This will fail with NameError: name 'second_function' is not defined
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

def first_function():
    print("Inside first function")
    second_function()

first_function()

def second_function():
    print("Inside second function")

