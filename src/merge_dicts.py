h_first = {'A': 1, 'B': 2, 'C': 3}
h_second = {'D': 4, 'E': 5, 'F': 6, 'G': 7, 'A': 9999, 'Z': 9999999999999999999999999999999999999999999999999999999999999}

h_combined = {**h_first, **h_second}

# Making a copy
h_third = h_first.copy()
h_fourth = dict(h_third)

# Update the first with the second
h_first.update(h_second)

# Using kwargs
h_another_combined = dict(h_third, **h_second)

print(h_combined)
print(h_first)

# With Collections
from collections import ChainMap

h_new = dict(ChainMap(h_third, h_second))

# With itertools
from itertools import chain

# Use chain constructor
h_new2 = dict(chain(h_third.items(), h_second.items()))

# Merge, only for Python 3.9 and newer
# h_new3 = h_third | h_second
# print(h_new3)

# Interesting examples in:
# https://www.javatpoint.com/merge-two-dictionaries-in-python

