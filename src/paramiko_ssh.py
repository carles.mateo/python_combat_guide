import paramiko

# Install paramiko pip3 module first


def ssh_and_run_remote_command(s_host, s_ssh_user, s_ssh_password, s_command):
    """
    SSH into a host and execute the command.
    :param str s_host:
    :param str s_ssh_user:
    :param str s_ssh_password:
    :param str s_command:
    :return: b_success, s_output, s_error
    """

    b_success = True
    s_output = ""
    s_error = ""
    b_connection_open = False

    try:

        o_ssh_client = paramiko.SSHClient()
        o_ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        o_ssh_client.connect(hostname=s_host,
                           username=s_ssh_user,
                           password=s_ssh_password)
        b_connection_open = True
        o_stdin, o_stdout, o_stderr = o_ssh_client.exec_command(s_command)

        s_output = o_stdout.read().decode().strip()
        s_error = o_stderr.read().decode().strip()

    except:
        b_success = False

    if b_connection_open is True:
        o_ssh_client.close()

    return b_success, s_output, s_error


if __name__ == "__main__":
    s_target_host = "192.168.0.10"
    s_target_username = "carles"
    s_target_password = "secretPassword"
    s_target_command = "df -h /"
    print("SSHing to " + s_target_host + " with user: " + s_target_username + " to execute: " + s_target_command)
    b_success, s_output, s_error = ssh_and_run_remote_command(s_target_host, s_target_username, s_target_password, s_target_command)

    if b_success is True:
        print("Command successful. Output:\n")
        print(s_output)
    else:
        print("Command execution failed\n")
        if len(s_error) > 0:
            print(s_error)
