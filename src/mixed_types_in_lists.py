a_remember = ["buy milk", 3.1416, 1637]

for m_info in a_remember:
    print(m_info)

h_remember = {}
h_remember["supermarket"] = "buy milk"
h_remember[3.14] = "Pi"
h_remember[1637] = "Time to code more!"

for m_key in h_remember:
    print(h_remember[m_key])
