
if __name__ == "__main__":
    cars = [
      {'car': 'Ford', 'year': 2005},
      {'car': 'Mitsubishi', 'year': 2000},
      {'car': 'BMW', 'year': 2019},
      {'car': 'VW', 'year': 2011},
      {'car': 'Phantom', 'year': None}
    ]

    cars.sort(key=lambda x: x['year'])
    print(cars)
