# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.

from lib.subprocesses import SubProcesses


def print_what_happened(s_cmd, i_error_code, s_output, s_error):
    if i_error_code == 0:
        print("Command (" + s_cmd + ") was executed successfully with output:")
        print(s_output)
    else:
        print("Command (" + s_cmd + ") failed with error code: " + str(i_error_code))
        if s_error != "":
            print("STDERR Contains information: " + s_error)

o_subprocesses = SubProcesses()

s_cmd_smartctl = "smartctl -i /dev/nvme0n1"

i_error_code, s_output, s_error = o_subprocesses.execute_command_for_output(s_command=s_cmd_smartctl)
print_what_happened(s_cmd_smartctl, i_error_code, s_output, s_error)

s_cmd_modinfo_zfs = "modinfo zfs -F filename"
i_error_code, s_output, s_error = o_subprocesses.execute_command_for_output(s_command=s_cmd_modinfo_zfs)
print_what_happened(s_cmd_modinfo_zfs, i_error_code, s_output, s_error)
