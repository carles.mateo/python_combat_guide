#!/bin/python3

import math
import os
import random
import re
import sys

"""
5 9
#%$r%r$n 
O%Mi$iTi$
yiaxsprt 
est%ctiy#
  t c i %

Expected Output

#Oye is Mattrix sccript Triinity  $ #%
"""

"""
2 2
# 
 @
 
Expected Output
Download

#  @
"""

"""
2 4
# i#
 @#U

Expected Output
Download

#  @i U
"""

"""
7 3
Tsi
h%x
i #
sM 
$a 
#t%
ir!
"""

first_multiple_input = input().rstrip().split()

i_rows = int(first_multiple_input[0])

i_columns = int(first_multiple_input[1])

a_matrix = []
s_pattern = '[a-zA-Z0-9!@#$%&]'
s_pattern_last_character= "[a-zA-Z0-9]"

for _ in range(i_rows):
    s_matrix_item = input()
    a_submatrix = []
    for i_x in range(0, i_columns):
        #matrix.append(matrix_item)
        a_submatrix.append(s_matrix_item[i_x])
    a_matrix.append(a_submatrix)

i_final_pos = 0
i_first_pos = 0

a_positions = []
for i_y, a_submatrix in enumerate(a_matrix):
    for i_x in range(0, i_columns):
        s_char = a_submatrix[i_x]
        # Filter one char
        a_new_char = re.findall(s_pattern, s_char)
        try:
            s_temp_new_char = a_new_char[0]
        except:
        #if len(a_new_char) == 0:
            # Replace with space
            a_new_char.append(" ")
        a_submatrix[i_x] = a_new_char[0]
        a_matrix[i_y] = a_submatrix
        # Find the last letter
        a_new_char = re.findall(s_pattern_last_character, s_char)
        #if len(a_new_char) > 0:
        for s_new_char in a_new_char:
            #i_this_pos = i_x + (i_rows * i_x) #+ (i_y * i_columns)
            i_this_pos = i_x + (i_x * i_rows) #(i_y  * i_columns)  # + (i_y * i_columns)
            a_positions.append(i_this_pos)
            #if i_this_pos > i_final_pos:
                #i_final_pos = i_this_pos

a_positions.sort(reverse=True)
for i_tpm_number in a_positions:
    i_final_pos = a_positions[0]
    i_first_pos = a_positions[len(a_positions) - 1]

s_final_string = ""
for i_x in range(0, i_columns):
    for i_y, a_submatrix in enumerate(a_matrix):
        s_final_string += a_submatrix[i_x]

#x = re.search(s_pattern_last_character, s_final_string)
#i_final_pos = x.span()[1]
s_pre_pre_final_string = s_final_string[0:i_first_pos]
s_pre_final_string = s_final_string[i_first_pos:i_final_pos]
s_pre_final_string = re.sub('[!@#$%&]', " ", s_pre_final_string)

b_exit = False
while b_exit is False:
    b_exit = True
    a_spaces_found = re.findall("\s\s", s_pre_final_string)
    for s_spaces in a_spaces_found:
        b_exit = False
        s_pre_final_string = re.sub("\s\s", " ", s_pre_final_string)
s_post_final_string = s_final_string[i_final_pos:]
s_final_string = s_pre_pre_final_string + s_pre_final_string + s_post_final_string

print(s_final_string)
