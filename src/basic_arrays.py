# For my book Python Exercises for Beginners
# https://leanpub.com/python3-exercises-beginners

a_list = [324, 367, 876, 8, 9, 9845, 777, 9, 456, 34, 65]


def print_list_with_while():
    i_index = 0
    while i_index < len(a_list):
        print(a_list[i_index])
        i_index = i_index + 1


def print_list_with_for():
    for i_number in a_list:
        print(i_number)


def print_sorted_without_using_sort():

    b_continue = True

    # We set a very big value bigger than any element in the array, so we will check for anything below that number.
    # Assuming the Array only contains positive numbers and no duplications
    i_min_value_found_in_loop = 100000000000
    i_min_value_printed = -1
    while b_continue is True:
        # Next loop will end unless in the for we found a value not printed yet
        b_continue = False
        for i_number in a_list:
            if i_number > i_min_value_printed:
                if i_number < i_min_value_found_in_loop:
                    i_min_value_found_in_loop = i_number
                    b_continue = True

        if b_continue is True:
            print(i_min_value_found_in_loop)
            i_min_value_printed = i_min_value_found_in_loop
            i_min_value_found_in_loop = 100000000000


if __name__ == "__main__":

    # a_list.sort()
    # print_list_with_for()

    print_sorted_without_using_sort()


