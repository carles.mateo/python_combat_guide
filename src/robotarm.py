from baserobot import BaseRobot


class RobotArm(BaseRobot):

    i_base_horizontal = 0
    i_base_vertical = 0
    i_angle_hand = 0
    i_angle_clamp = 0

    def get_base_horizontal(self):
        return self.i_base_horizontal

    def get_base_vertical(self):
        return self.i_base_vertical

    def get_angle_hand(self):
        return self.i_angle_hand

    def get_angle_clamp(self):
        return self.i_angle_clamp

    def move_base_horizontal_right(self, i_degrees=1):
        # Do the action
        self.i_base_horizontal = self.i_base_horizontal + 1
        if self.i_base_horizontal > 359:
            self.i_base_horizontal = 0

    # Ohter Methods here

    def __init__(self, s_serial_number, s_color, s_model_name, s_date_build):
        self.set_serial_number(s_serial_number=s_serial_number)
        self.set_color(s_color=s_color)
        self.set_model_name(s_model_name=s_model_name)
        self.set_date_built(s_date_built=s_date_build)


o_robotarm = RobotArm(s_serial_number="1234567X", s_color="metal", s_model_name="T-800", s_date_build="2050-07-25")
print(o_robotarm.s_model_name)
print(BaseRobot.s_model_name)
print(o_robotarm.s_mission)
