#!/usr/bin/env python3

#
# staticdynamic.py
#
# Author: Carles Mateo
# Creation Date: 2021-01-19
# Description: A sample to demonstrate Static Variables in Python.
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

class Disk:
    s_name = ""
    s_type = ""
    s_vendor = ""
    h_s_i_partitions = {}

    def __init__(self, s_name, s_type, s_vendor):
        self.s_name = s_name
        self.s_type = s_type
        self.s_vendor = s_vendor
        # Uncomment to an instance variable is created
        # self.h_s_i_partitions = {}

    def add_partition(self, s_part, i_size):
        self.h_s_i_partitions[s_part] = i_size


if __name__ == '__main__':
    o_disk = Disk("sda", "disk", "Samsung")
    o_disk.add_partition("sda1", 512)
    o_disk.add_partition("sda2", 256)
    o_disk2 = Disk("sdb", "disk", "Samsung")
    o_disk2.add_partition("sdb1", 128)
    o_disk3 = Disk("nvme0n1", "disk", "Samsung")
    o_disk3.add_partition("nvme0n1p1", 256)
    o_disk3.add_partition("nvme0n1p2", 256)

    print(o_disk.h_s_i_partitions)
    # Note: This prints {'sda1': 512, 'sda2': 256, 'sdb1': 128, 'nvme0n1p1': 256, 'nvme0n1p2': 256}
    #       That's cause h_s_i_partitions is considered Static by Python as we have not overwritten it.
    print(o_disk2.h_s_i_partitions)
    print(o_disk3.h_s_i_partitions)

    print()
    print("Value in the static Class variable:")
    print(Disk.h_s_i_partitions)
