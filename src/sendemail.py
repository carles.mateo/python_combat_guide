#!/usr/bin/env python3
#
# sendemail.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: Sample file to show the use of smtputils.
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

from lib.smtputils import SmtpUtils

s_smtp_server_host = "127.0.0.1"
i_smtp_server_port = 25
i_smtp_server_timeout = 5
o_smtputils = SmtpUtils(s_smtp_server_host=s_smtp_server_host,
                        i_smtp_server_port=i_smtp_server_port,
                        i_smtp_server_timeout=i_smtp_server_timeout)

s_from_addr = "carles@carlesmateo.com"
s_to_addrs = "carles.mateo@gmail.com; carles@carlesmateo.com"
s_msg = "This is a Test"
b_success_total, b_email_sent, h_senderrs = o_smtputils.send_email(s_from_addr=s_from_addr,
                                                                   s_to_addrs=s_to_addrs,
                                                                   s_msg=s_msg)

if b_success_total is True:
    print("The process of sending email and closing the connection worked")
else:
    print("The process of sending email partially worked, some users may have received the email " \
          "or maybe the quit command failed")
    print(h_senderrs)
    if b_email_sent is True:
        print("However the email was successfully sent")
