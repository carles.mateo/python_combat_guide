#!/usr/bin/python3
#
# flaskhelloworld.py
#
# Author: Carles Mateo
# Creation Date: 2021-04-02 14:12 Iralend
# Description: A simple Flask Web Application
#              Part of the samples of https://leanpub.com/pythoncombatguide
#              More source code for the book at https://gitlab.com/carles.mateo/python_combat_guide
#
# Instructions for Linux:
# export FLASK_APP=flaskhelloworld.py
# flask run


from flask import Flask

# Initialize Flask
app = Flask(__name__)

# Sample route so http://127.0.0.1/
@app.route('/', methods=['GET'])
def homepage():

    return "<h1>Hello World!</h1>"

# This is the same as the decorator.
# app.add_url_rule('/', 'index', homepage)