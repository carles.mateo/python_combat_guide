#!/usr/bin/env python3

from lib.pylibsubprocess import PyLibSubProcess


class Disk:
    s_name = ""
    s_hctl = ""
    s_type = ""
    s_tran = ""
    s_vendor = ""
    s_model = ""
    s_rev = ""
    s_fssize = ""
    s_fstype = ""
    s_fsused = ""
    s_fuse_pct = ""
    s_mountpoint = ""
    h_s_o_partitions = {}

    def __init__(self, s_name, s_hctl, s_type, s_tran, s_vendor, s_model, s_rev, s_fssize, s_fstype, s_fsused,
                 s_fuse_pct, s_mountpoint):
        self.s_name = s_name
        self.s_hctl = s_hctl
        self.s_type = s_type
        self.s_tran = s_tran
        self.s_vendor = s_vendor
        self.s_model = s_model
        self.s_rev = s_rev
        self.s_fssize = s_fssize
        self.s_fstype = s_fstype
        self.s_fsused = s_fsused
        self.s_fuse_pct = s_fuse_pct
        self.s_mountpoint = s_mountpoint
        self.h_s_o_partitions = {}

    def add_partition(self, o_part):
        self.h_s_o_partitions[o_part.s_name] = o_part


class Partition:
    s_name = ""
    s_type = ""
    s_fssize = ""
    s_fstype = ""
    s_fsused = ""
    s_fuse_pct = ""
    s_mountpoint = ""

    def __init__(self, s_name, s_type, s_fssize, s_fstype, s_fsused,
                 s_fuse_pct, s_mountpoint):
        self.s_name = s_name
        self.s_type = s_type
        self.s_fssize = s_fssize
        self.s_fstype = s_fstype
        self.s_fsused = s_fsused
        self.s_fuse_pct = s_fuse_pct
        self.s_mountpoint = s_mountpoint


class StringUtils:

    a_s_find = ["NAME", "HCTL", "TYPE", "TRAN", "VENDOR", "MODEL", "REV", "FSSIZE", "FSTYPE", "FSUSED", "FSUSE%",
                "MOUNTPOINT"]

    def convert_string_to_array(self, s_text):
        a_lines = s_text.split("\n")

        return a_lines

    def extract_index_titles(self, a_s_lines):
        """
        Will check for errors and make sure the structure is right.
        Will find the index for each of fields we are looking for in the first line of the array of str passed.
        :param a_s_lines:
        :return: b_error, h_s_i_fields
        """
        b_error_fields = False

        h_s_i_fields = {}

        if len(a_s_lines) < 1:
            b_error_fields = True
        else:
            i_old_pos = 0
            i_field = 0
            for s_field in self.a_s_find:
                i_position = a_s_lines[0].find(s_field)
                if (i_field == 0 and i_position == i_old_pos) or (i_field > 0 and i_position > i_old_pos):
                    # Found and is after the previous, except the first field which is at position 0
                    h_s_i_fields[s_field] = i_position
                    i_old_pos = i_position
                else:
                    b_error_fields = True
                    break
                i_field = i_field + 1

        return b_error_fields, h_s_i_fields

    def extract_disks(self, a_s_lines, h_s_i_fields):
        h_s_o_disks = {}

        i_line = 0
        for s_line in a_s_lines:
            if i_line == 0:
                # First line is titles
                i_line = i_line + 1
                continue

            s_name = s_line[h_s_i_fields["NAME"]:h_s_i_fields["HCTL"]-1].rstrip()
            s_htcl = s_line[h_s_i_fields["HCTL"]:h_s_i_fields["TYPE"]-1].rstrip()
            s_type = s_line[h_s_i_fields["TYPE"]:h_s_i_fields["TRAN"]-1].rstrip()
            s_tran = s_line[h_s_i_fields["TRAN"]:h_s_i_fields["VENDOR"]-1].rstrip()
            s_vendor = s_line[h_s_i_fields["VENDOR"]:h_s_i_fields["MODEL"]-1].rstrip()
            s_model = s_line[h_s_i_fields["MODEL"]:h_s_i_fields["REV"]-1].rstrip()
            s_rev = s_line[h_s_i_fields["REV"]:h_s_i_fields["FSSIZE"]-1].rstrip()
            s_fssize = s_line[h_s_i_fields["FSSIZE"]:h_s_i_fields["FSTYPE"]-1].rstrip()
            s_fstype = s_line[h_s_i_fields["FSTYPE"]:h_s_i_fields["FSUSED"]-1].rstrip()
            s_fsused = s_line[h_s_i_fields["FSUSED"]:h_s_i_fields["FSUSE%"]-1].rstrip()
            s_fuse_pct = s_line[h_s_i_fields["FSUSE%"]:h_s_i_fields["MOUNTPOINT"]-1].rstrip()
            s_mountpoint = s_line[h_s_i_fields["MOUNTPOINT"]:]

            if s_type == "disk":
                # We ignore the loops, only work with disks
                o_disk = Disk(s_name, s_htcl, s_type, s_tran, s_vendor, s_model, s_rev, s_fssize, s_fstype, s_fsused,
                              s_fuse_pct, s_mountpoint)
                h_s_o_disks[s_name] = o_disk
            elif s_type == "part":
                o_part = Partition(s_name, s_type, s_fssize, s_fstype, s_fsused, s_fuse_pct, s_mountpoint)
                # print("Added partition: " + s_name + " to " + o_disk.s_name)
                # We use the previous stored disk object
                o_disk.add_partition(o_part)

            i_line = i_line + 1

        return h_s_o_disks


if __name__ == '__main__':
    h_s_o_disks = {}
    o_pylibsubprocess = PyLibSubProcess()

    a_command = ['lsblk', '-o', 'NAME,HCTL,TYPE,TRAN,VENDOR,MODEL,REV,FSSIZE,FSTYPE,FSUSED,FSUSE%,MOUNTPOINT']
    b_success, b_timeout, s_stdout, s_stderr = o_pylibsubprocess.run(a_command, i_timeout=10)

    if b_success is True:

        o_stringutils = StringUtils()

        a_lines = o_stringutils.convert_string_to_array(s_stdout)
        b_error, h_s_i_index_titles = o_stringutils.extract_index_titles(a_s_lines=a_lines)
        if b_error is True:
            print("Error. The output of lsblk is unexpected")
            exit(1)

        h_s_o_disks = o_stringutils.extract_disks(a_lines, h_s_i_index_titles)

        # Ini: --------------------------- This is the old code ---------------------------
        # print("Output from lsblk except loop")
        # for s_line in a_lines:
        #     if s_line[0:4] == "loop":
        #         # Is a loop device, ignore it
        #         continue
        #
        #     print(s_line)
        # End: --------------------------- This is the old code ---------------------------

        print("Print from the objects new functionality")
        i_disks = len(h_s_o_disks)
        print(str(i_disks) + " disks founds")

        for s_name in h_s_o_disks:
            o_disk = h_s_o_disks[s_name]
            i_partitions = len(o_disk.h_s_o_partitions)

            print(o_disk.s_name + " " + o_disk.s_tran + " " + o_disk.s_model + " " " Partitions: " + str(i_partitions))

    else:
        print("There has been an error: " + s_stderr)
        exit(1)
