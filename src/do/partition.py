#
# partition.py
#
# Author: Carles Mateo
# Creation Date: 2021-01-24 22:42 Ireland
# Description: Data Object Partition
#

class Partition:
    s_name = ""
    s_type = ""
    s_fssize = ""
    s_fstype = ""
    s_fsused = ""
    s_fuse_pct = ""
    s_mountpoint = ""

    def __init__(self, s_name, s_type, s_fssize, s_fstype, s_fsused,
                 s_fuse_pct, s_mountpoint):
        self.s_name = s_name
        self.s_type = s_type
        self.s_fssize = s_fssize
        self.s_fstype = s_fstype
        self.s_fsused = s_fsused
        self.s_fuse_pct = s_fuse_pct
        self.s_mountpoint = s_mountpoint
