#
# disk.py
#
# Author: Carles Mateo
# Creation Date: 2021-01-24 22:40 Ireland
# Description: Data Object Disk
#

class Disk:
    s_name = ""
    s_hctl = ""
    s_type = ""
    s_tran = ""
    s_vendor = ""
    s_model = ""
    s_rev = ""
    s_fssize = ""
    s_fstype = ""
    s_fsused = ""
    s_fuse_pct = ""
    s_mountpoint = ""
    h_s_o_partitions = {}

    def __init__(self, s_name, s_hctl, s_type, s_tran, s_vendor, s_model, s_rev, s_fssize, s_fstype, s_fsused,
                 s_fuse_pct, s_mountpoint):
        self.s_name = s_name
        self.s_hctl = s_hctl
        self.s_type = s_type
        self.s_tran = s_tran
        self.s_vendor = s_vendor
        self.s_model = s_model
        self.s_rev = s_rev
        self.s_fssize = s_fssize
        self.s_fstype = s_fstype
        self.s_fsused = s_fsused
        self.s_fuse_pct = s_fuse_pct
        self.s_mountpoint = s_mountpoint
        self.h_s_o_partitions = {}

    def add_partition(self, o_part):
        self.h_s_o_partitions[o_part.s_name] = o_part
