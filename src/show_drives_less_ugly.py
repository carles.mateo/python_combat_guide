#!/usr/bin/env python3

from lib.pylibsubprocess import PyLibSubProcess


def convert_string_to_array(s_text):
    a_lines = s_text.split("\n")

    return a_lines


if __name__ == '__main__':
    o_pylibsubprocess = PyLibSubProcess()

    a_command = ['lsblk', '-o', 'NAME,HCTL,TYPE,TRAN,VENDOR,MODEL,REV,FSSIZE,FSTYPE,FSUSED,FSUSE%,MOUNTPOINT']
    b_success, b_timeout, s_stdout, s_stderr = o_pylibsubprocess.run(a_command, i_timeout=10)

    if b_success is True:

        a_lines = convert_string_to_array(s_stdout)

        for s_line in a_lines:
            if s_line[0:4] == "loop":
                # Is a loop device, ignore it
                continue

            print(s_line)

    else:
        print("There has been an error: " + s_stderr)
