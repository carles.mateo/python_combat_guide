#!/usr/bin/env python3
#
# for_range.py
#
# Author: Carles Mateo
# Creation Date: 2021-01-21
# Description: Show a for range() sample, to discuss how different works respect languages
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

if __name__ == '__main__':

    x = range(10000000)

    i_total = 0
    for i_number in range(0, 1000):
        i_total = i_total + i_number

    print("Total summing first range: ", i_total)

    i_total = 0
    for i_number in x:
        i_total += i_number

    print("Total summing second range: ", i_total)
    print("Length of range() x: ", len(x))
    print("The range is really an array/list.")
    print(x)
    print(x[100])

    x = range(3, 10000000, 7)
    print(x[53])

    print(10 in x)
    print(4 in x)
    print(109458495800001 in x)
