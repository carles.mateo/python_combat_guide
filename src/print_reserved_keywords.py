#!/usr/bin/env python

# Code from: https://zetcode.com/lang/python/keywords/
# keywords.py

import sys
import keyword

print("Python version: ", sys.version_info)
print("Python keywords: ", keyword.kwlist)
