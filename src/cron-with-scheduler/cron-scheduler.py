#!/usr/bin/env python3

import logging
from apscheduler.schedulers.blocking import BlockingScheduler  # pip install APScheduler


s_program_name = "cron-scheduler"
s_log_file = "cron-scheduler.log"

# Enabling basic logging
logging.basicConfig(filename=s_log_file, format='%(asctime)s %(message)s',
                    level=logging.DEBUG)


def cron_scheduler():
    print(s_program_name + " is running...")
    logging.debug(s_program_name + " is running...")
    # @TODO: Do your actions here


def main():

    # Hours to trigger the scheduler
    s_run_at = '4,10,12,16,20'

    # Set the scheduler to LA timezone
    o_scheduler = BlockingScheduler(timezone='America/Los_Angeles')
    o_scheduler.add_job(cron_scheduler, trigger='cron', hour=s_run_at, minute='0')
    print('Press Ctrl + C to exit program')

    try:
        o_scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == "__main__":
    main()
