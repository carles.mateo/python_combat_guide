from colorama import Fore, Back, Style , init


def color_blue(s_text):
    return Fore.BLUE + s_text + Fore.RESET


def print_header():
    s_student_bw = "Student".rjust(10)
    s_student_color = color_blue(s_student_bw)
    s_specialty_bw = "Specialty".rjust(11)
    s_specialty_color = color_blue(s_specialty_bw)
    s_score_bw = "Score".rjust(5)
    s_score_color = color_blue(s_score_bw)
    s_place_bw = "Place".rjust(20)
    s_place_color = color_blue(s_place_bw)

    print(s_student_color, s_specialty_color, s_score_color, s_place_color)
    print(color_blue("=" * len(s_student_bw)), color_blue("=" * len(s_specialty_bw)), color_blue("=" * len(s_score_bw)),
          color_blue("=" * len(s_place_bw)))


def print_students(a_students):

    for t_student in a_students:
        s_student_name = t_student[0]
        s_student_specialty = t_student[1]
        i_student_score = t_student[2]
        s_place = t_student[3]

        print(s_student_name.rjust(10), s_student_specialty.rjust(11), str(i_student_score).rjust(5), s_place.rjust(20))

    print()


def sort_by_column(a_students, i_index, b_reverse = True):

    a_students.sort(key=lambda x: x[i_index].lower().replace("à", "a").replace("è", "e").replace("ú", "u"), reverse=b_reverse)


if __name__ == "__main__":

    a_students = []

    # For Colorama
    init()

    # Student, Specialty, Score
    t_student1 = ("Jaume", "Python", 99, "Barcelona")
    t_student2 = ("Laia", "PHP", 85, "Girona")
    t_student3 = ("Albert", "PHP", 100, "Tarragona")
    t_student4 = ("Sid", "Python", 80, "València")
    t_student5 = ("Michela", "Python", 89, "Alguer")
    t_student6 = ("Alex", "Python", 91, "Cànoves i Samalús")
    t_student7 = ("Sandra", "Javascript", 91, "Cardona")
    t_student8 = ("Magda", "Javascript", 90, "Vallirana")
    t_student9 = ("Colm", "Javascript", 90, "Andorra")

    a_students.append(t_student1)
    a_students.append(t_student2)
    a_students.append(t_student3)
    a_students.append(t_student4)
    a_students.append(t_student5)
    a_students.append(t_student6)
    a_students.append(t_student7)
    a_students.append(t_student8)
    a_students.append(t_student9)

    sort_by_column(a_students, 3, b_reverse=False)

    print_header()
    print_students(a_students)

