class BaseRobot:

    s_serial_number = "N/A"
    s_color = "N/A"
    s_model_name = "N/A"
    s_date_built = "N/A"
    s_mission = "Conquer the World"

    def get_serial_number(self):
        return self.s_serial_number

    def get_color(self):
        return self.s_color

    def get_model_name(self):
        return self.s_model_name

    def get_date_built(self):
        return self.s_date_built

    def set_serial_number(self, s_serial_number):
        self.s_serial_number = s_serial_number

    def set_color(self, s_color):
        self.s_color = s_color

    def set_model_name(self, s_model_name):
        self.s_model_name = s_model_name

    def set_date_built(self, s_date_built):
        self.s_date_built = s_date_built
