#!/usr/bin/env python3

from lib.pylibsubprocess import PyLibSubProcess
from lib.fileutils import FileUtils
from lib.stringutils import StringUtils
from do.disk import Disk
from do.partition import Partition


class Drives:

    # Constant. Variables to catch from lsblk output
    a_s_FIND = ["NAME", "HCTL", "TYPE", "TRAN", "VENDOR", "MODEL", "REV", "FSSIZE", "FSTYPE", "FSUSED", "FSUSE%",
                "MOUNTPOINT"]

    # Libraries
    o_stringutils = None
    o_fileutils = None

    # Dictionary to store the Disks
    h_s_o_disks = {}

    def __init__(self, o_stringutils, o_fileutils):
        self.o_stringutils = o_stringutils
        self.o_fileutils = o_fileutils
        self.h_s_o_disks = {}


    def extract_disks(self, a_s_lines, h_s_i_fields):
        h_s_o_disks = {}

        i_line = 0
        for s_line in a_s_lines:
            if i_line == 0:
                # First line is titles
                i_line = i_line + 1
                continue

            h_s_s_data = self.o_stringutils.extract_data_from_index(s_line, self.a_s_FIND, h_s_i_fields)

            s_name = h_s_s_data["NAME"].lstrip('├─').lstrip('└─')
            s_htcl = h_s_s_data["HCTL"]
            s_type = h_s_s_data["TYPE"]
            s_tran = h_s_s_data["TRAN"]
            s_vendor = h_s_s_data["VENDOR"]
            s_model = h_s_s_data["MODEL"]
            s_rev = h_s_s_data["REV"]
            s_fssize = h_s_s_data["FSSIZE"]
            s_fstype = h_s_s_data["FSTYPE"]
            s_fsused = h_s_s_data["FSUSED"]
            s_fuse_pct = h_s_s_data["FSUSE%"]
            s_mountpoint = h_s_s_data["MOUNTPOINT"]

            if s_type == "disk":
                # We ignore the loops, only work with disks
                o_disk = Disk(s_name, s_htcl, s_type, s_tran, s_vendor, s_model, s_rev, s_fssize, s_fstype, s_fsused,
                              s_fuse_pct, s_mountpoint)
                h_s_o_disks[s_name] = o_disk
            elif s_type == "part":
                o_part = Partition(s_name, s_type, s_fssize, s_fstype, s_fsused, s_fuse_pct, s_mountpoint)
                # We use the previous stored disk object
                o_disk.add_partition(o_part)

            i_line = i_line + 1

        return h_s_o_disks

    def get_disks(self):
        """
        Return the disks as a dictionary
        :return: b_error, h_s_o_disks
        """
        h_s_o_disks = {}
        b_error = False


        b_success, a_ls = self.o_fileutils.get_all_files_or_directories_with_mask("/dev/disk/by-id/*")
        if b_success is True:
            print(a_ls)
        else:
            b_error = True
        exit(1)

        a_command = ["lsblk", "-o", ",".join(self.a_s_FIND)]
        b_success, b_timeout, s_stdout, s_stderr = self.o_pylibsubprocess.run(a_command, i_timeout=10)

        if b_success is True:

            a_lines = self.o_stringutils.convert_string_to_array(s_stdout)
            b_error_titles, h_s_i_index_titles = self.o_stringutils.extract_index_titles(self.a_s_FIND, a_s_lines=a_lines, h_s_i_fields={})

            if b_error_titles is True:
                # Error. The output of lsblk is unexpected
                b_error = True
            else:
                h_s_o_disks = self.extract_disks(a_lines, h_s_i_index_titles)
        else:
            b_error = True

        return b_error, h_s_o_disks

    def print_disks(self, h_s_o_disks):
        """
        Prints the disk on the screen
        :return:
        """
        i_disks = len(h_s_o_disks)
        print(str(i_disks) + " disks founds")

        for s_name in h_s_o_disks:
            o_disk = h_s_o_disks[s_name]
            i_partitions = len(o_disk.h_s_o_partitions)

            print(o_disk.s_name + " " + o_disk.s_tran + " " + o_disk.s_model + " Partitions: " + str(i_partitions))

    def main(self):
        b_error, h_s_o_disks = self.get_disks()

        if b_error is False:
            self.print_disks(h_s_o_disks)
        else:
            print("There has been an error.")
            exit(1)


if __name__ == '__main__':
    o_stringutils = StringUtils()
    o_fileutils = FileUtils()
    o_drives = Drives(o_stringutils, o_fileutils)
    o_drives.main()
