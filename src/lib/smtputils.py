#
# smtputils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: SmtpUtils class, to deal with email sending
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

import smtplib


class SmtpUtils:

    s_smtp_server_host = "127.0.0.1"
    i_smtp_server_port = 25
    i_smtp_server_timeout = 5

    def __init__(self, s_smtp_server_host="127.0.0.1", i_smtp_server_port=25, i_smtp_server_timeout=5):
        self.s_smtp_server_host = s_smtp_server_host
        self.i_smtp_server_port = i_smtp_server_port
        self.i_smtp_server_timeout = i_smtp_server_timeout

    def get_smtp_connection(self):
        """
        Connects to a SMTP Server, says EHLO, returns the response
        :param s_smtp_server: The SMTP Server to connect
        :param i_port: The port number
        :param i_timeout: Timeout for the connection
        :return: bool: b_success
        :return: int: i_response: response code
        :return: str: s_msg: Message from the Server
        """

        b_success = True

        try:
            o_smtp_connection = smtplib.SMTP(self.s_smtp_server_host, self.i_smtp_server_port, self.i_smtp_server_timeout)
        except:
            b_success = False

        return b_success, o_smtp_connection

    def send_email_though_connection(self, o_smtp_connection, s_from_addr, s_to_addrs, s_msg, t_mail_options=(), t_rcpt_options=()):
        """
        Sends an email
        :param o_smtp_connection:
        :param s_from_addr: Sender email
        :param s_to_addrs: Receivers emails, separated by semicolon
        :param s_msg: The message
        :param t_mail_options:
        :param t_rcpt_options:
        :return: bool: b_success
        :return: arrayhash: h_senderrs
        """

        b_success = True
        h_senderrs = {}

        try:
            h_senderrs = o_smtp_connection.sendmail(s_from_addr, s_to_addrs, s_msg, mail_options=t_mail_options, rcpt_options=t_rcpt_options)
        except smtplib.SMTPSenderRefused:
            h_senderrs["smtpsenderrefused"] = "The Sender " + s_from_addr + " has been refused by " + self.s_smtp_server_host
            b_success = False
        except:
            b_success = False

        return b_success, h_senderrs

    def quit_smtp_connection(self, o_smtp_connection):
        """
        Closes the connection
        :param o_smtp_connection:
        :return: bool: b_success
        """

        b_success = True

        try:
            o_smtp_connection.quit()
        except:
            b_success = False

        return b_success

    def send_email(self, s_from_addr, s_to_addrs, s_msg):
        """
        Attempts all the steps
        :param s_from_addr:
        :param s_to_addrs:
        :param s_msg:
        :return: bool: b_success_total: If all the process worked smoothly
        :return: bool: b_success_email_sent: If the email was sent, even if only one or quit failed
        :return: arrayhash: h_senderrs: list of errors
        """
        b_success_total = False
        b_success_email_sent = False

        b_success, o_smtp_connection = self.get_smtp_connection()


        if b_success is True:
            b_success, h_senderrs = self.send_email_though_connection(o_smtp_connection, s_from_addr, s_to_addrs, s_msg,
                                                                      t_mail_options=(), t_rcpt_options=())
            if b_success is True:
                b_success_email_sent = True
                b_success = self.quit_smtp_connection(o_smtp_connection)
                if b_success is True and len(h_senderrs) == 0:
                    b_success_total = True

        return b_success_total, b_success_email_sent, h_senderrs
