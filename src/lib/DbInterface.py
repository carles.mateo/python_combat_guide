#
# Database Interface Sample File
#
# Author: Carles Mateo
# Creation Date: 2017-06-01
# Description: Interface to work with Databases and implementations.
#              This is a simplified version to work with SQLite3 only
#


import abc


class DbInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        """
        We use the subclass Hook so when we have a request like issubclass(DbMySQL, DbInterface)
        we will return True if all the four methods are implemented.
        """

        return (hasattr(subclass, 'db_connect') and
                callable(subclass.db_connect) and
                hasattr(subclass, 'db_disconnect') and
                callable(subclass.db_disconnect) and
                hasattr(subclass, 'db_query_read') and
                callable(subclass.db_query_read) and
                hasattr(subclass, 'db_query_write') and
                callable(subclass.db_query_write) or
                NotImplemented)

    @abc.abstractmethod
    def db_connect(self, s_connection_string: str):
        """Connect to the Database"""
        raise NotImplementedError

    @abc.abstractmethod
    def db_disconnect(self):
        """Disconnect from the Database"""
        raise NotImplementedError

    @abc.abstractmethod
    def db_query_read(self, s_query_read: str):
        """Query the Database for read"""
        raise NotImplementedError

    @abc.abstractmethod
    def db_query_write(self, s_query_write: str):
        """Query the Database for write"""
        raise NotImplementedError


class DbMySQL(DbInterface):
    """MySQL Database functionality"""

    def db_connect(self, s_connection_string: str) -> str:
        """Overrides DbInterface.db_connet()"""
        # @TODO: Implement your code here
        pass

    def db_disconnect(self, s_connection_string: str) -> str:
        """Overrides DbInterface.db_disconnect()"""
        # @TODO: Implement your code here
        pass

    def db_query_read(self, full_file_path: str) -> dict:
        """Overrides DbInterface.db_query_read()"""
        # @TODO: Implement your code here
        pass

    def db_query_write(self, s_query_write: str) -> dict:
        """Overrides DbInterface.db_query_write()"""
        pass


class DbOracle(DbInterface):
    """Oracle Database functionality"""

    def db_connect(self, s_connection_string: str) -> str:
        """Overrides DbInterface.db_connet()"""
        # @TODO: Implement your code here
        pass

    def db_disconnect(self, s_connection_string: str) -> str:
        """Overrides DbInterface.db_disconnect()"""
        # @TODO: Implement your code here
        pass

    def db_query_read(self, full_file_path: str) -> dict:
        """Overrides DbInterface.db_query_read()"""
        # @TODO: Implement your code here
        pass

    def db_query_write(self, s_query_write: str) -> dict:
        """Overrides DbInterface.db_query_write()"""
        pass
