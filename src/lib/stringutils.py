#
# stringutils.py
#
# Author: Carles Mateo
# Creation Date: 2017-07-01
# Description: StringUtils class, to deal with string and number formatting
# Please Note: This code is used for discussing patterns in the guide, so wrong things could be there on purpose.
#

class StringUtils:

    def get_format_string_from_number(self, f_number):
        """
        Returns a string formatted, with 2 decimal places from a float
        :param f_number: Float or int number
        :return: str: A string formatted
        """

        s_nice_number = "{0:.2f}".format(f_number)

        return s_nice_number

    # Note: These two methods below were extracted from show_drives_oop_final.py
    def convert_string_to_array(self, s_text, s_separator="\n"):
        """
        Converts a text to String, separated by any character.
        :param s_separator:
        :param s_text:
        :return: a_lines
        """
        a_lines = s_text.split(s_separator)

        return a_lines

    def extract_index_titles(self, a_s_find, a_s_lines, h_s_i_fields):
        """
        Will find the index for each of fields we are looking for, in the first line of the array of str passed.
        Requirement: First field in h_s_i_fields must start in position 0.
        Will check for errors and make sure the structure is right.
        :param a_s_find:
        :param a_s_lines:
        :param h_s_i_fields:
        :return: b_error, h_s_i_fields
        """
        b_error_fields = False

        if len(a_s_lines) < 1:
            b_error_fields = True
        else:
            i_old_pos = 0
            i_field = 0
            for s_field in a_s_find:
                i_position = a_s_lines[0].find(s_field)
                if (i_field == 0 and i_position == i_old_pos) or (i_field > 0 and i_position > i_old_pos):
                    # Found and is after the previous, except the first field which is at position 0
                    h_s_i_fields[s_field] = i_position
                    i_old_pos = i_position
                else:
                    b_error_fields = True
                    break
                i_field = i_field + 1

        return b_error_fields, h_s_i_fields

    def extract_data_from_index(self, s_line, a_s_find, h_s_i_fields):
        """
        From a line of text, extracts the fields expected to be found.
        Example:
        a_s_find = ["NAME", "HCTL", "TYPE", "TRAN", "VENDOR", "MODEL", "REV", "FSSIZE", "FSTYPE", "FSUSED", "FSUSE%", "MOUNTPOINT"]
        :param s_line:
        :param a_s_find:
        :param h_s_i_fields:
        :return: h_s_s_data
        """
        h_s_s_data = {}
        b_error = False

        i_total_fields = len(a_s_find)
        for i_field, s_field in enumerate(a_s_find):

            i_pos_ini_field = h_s_i_fields[a_s_find[i_field]]

            if i_pos_ini_field > len(s_line):
                # Error: The line is shorter than the expected fields
                # We can return an error or consider that this means that the field should be blank
                # As this will be a widely used Lib, my choice is to return an empty string
                s_data = ""
            else:
                if i_field < i_total_fields - 1:
                    # Capture from the ini position to the ini position of the next field
                    i_pos_ini_next_field = h_s_i_fields[a_s_find[i_field + 1]]
                    i_pos_end_field = i_pos_ini_next_field - 1
                    s_data = s_line[i_pos_ini_field:i_pos_end_field]
                else:
                    # Is the last field. Capture to the end of the line
                    s_data = s_line[i_pos_ini_field:]

            # Remove ending spaces
            s_data = s_data.rstrip()

            h_s_s_data[s_field] = s_data

        return h_s_s_data
