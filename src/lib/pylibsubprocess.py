import subprocess


class PyLibSubProcess:

    def run(self, a_parameters, b_shell=False, i_timeout=60):
        """
        Will run the program indicated with the parameters
        :param a_parameters: Program on the first position of the array, arguments on the subsequents
        :param b_shell: If Shell will be invoked. Necessary to use Shell Scripts, pipes...
        :param i_timeout: Number of seconds before the process will be killed
        :return: b_success, b_timeout, s_stdout, s_stderr
        """
        b_success = True
        b_timeout = False
        s_stdout = ""
        s_stderr = ""

        try:
            o_execution = subprocess.run(a_parameters, shell=b_shell, capture_output=True, timeout=i_timeout)

        except subprocess.TimeoutExpired:
            b_timeout = True
            b_success = False
        except:
            b_success = False

        if b_success is True:
            s_stdout = "{}".format(o_execution.stdout.decode())
            s_stderr = "{}".format(o_execution.stderr.decode())

        return b_success, b_timeout, s_stdout, s_stderr
